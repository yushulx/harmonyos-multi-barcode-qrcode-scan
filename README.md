

## 鸿蒙应用：多码扫描
示例代码只为演示华为鸿蒙HarmonyOS多码扫描解决方案。

## 准备工作
1. 申请一个[免费试用序列号](https://www.dynamsoft.com/customer/license/trialLicense?product=dbr),设置到`server/app.js`中：
    
    ```javascript
    dbr.initLicense("LICENSE-KEY")
    ```
2. 安装Node.js依赖包，然后运行`server/app.js`：
    
    ```bash
    npm install
    node app.js
    ```

## 鸿蒙app运行方法
1. 把`client`工程导入到**DevEco Studio**。
2. 替换服务器地址和端口：
    
    ```typescript
    host: string = 'http://<ip>:<port>'
    ```
4. 运行工程，输入图片URL。先获取图片，然后传到服务端读取条码/二维码。
    
    ![鸿蒙多码扫描](https://devblogs.damingsoft.com/album/2023/10/harmonyos-multi-barcode-qrcode-scan.png)
    
